#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to get .xml
"""

import sys
import time
import os

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By

driver = webdriver.Remote(
         command_executor='http://selenium-standalone-firefox:4444/wd/hub',
         desired_capabilities=DesiredCapabilities.FIREFOX)

# driver = webdriver.Firefox(executable_path="/home/jenny/Documents/python_scripts.git/geckodriver")


def archivist_login(url, uname, pw):
    """
    Log in to archivist
    """
    driver.get(url)
    time.sleep(5)
    driver.find_element(By.ID, "email").send_keys(uname)
    driver.find_element(By.ID, "password").send_keys(pw)
    driver.find_element(By.XPATH, '//span[text()="Log In"]').click()
    time.sleep(5)


def archivist_download_xml(output_dir, uname, pw):
    """
    downloading the only xml
    """

    # Log in
    archivist_url = "https://closer-build.herokuapp.com"
    export_url = "https://closer-build.herokuapp.com/admin/instruments/exports"
    archivist_login(archivist_url, uname, pw)
    driver.get(export_url)
    time.sleep(5)

    # one and only one questionaire
    trs = driver.find_elements(By.XPATH, "html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")
    tr = trs[0]

    # column 2 is "Prefix"
    xml_prefix = tr.find_elements(by=By.XPATH, value="td")[1].text
    print(xml_prefix)

    # column 4 is "Actions"
    actions = tr.find_elements(By.XPATH, "td")[3].text

    # "Download export " text
    if len([item for item in actions.split('\n\n') if item.startswith('DOWNLOAD EXPORT')]) > 0:
        download_text = [item for item in actions.split('\n\n') if item.startswith('DOWNLOAD EXPORT')][0]
        xml_date = download_text.split('\n')[-1]

        # need to have download button
        exportButton = tr.find_elements(By.LINK_TEXT, download_text)
        xml_location = exportButton[0].get_attribute("href")
        #print(xml_location)
        #print(xml_date)
    else:
        xml_location = None
        xml_date = ''

    if not xml_location:
        raise ValueError("No download link for " + xml_prefix)

    print("Getting xml for " + xml_prefix)
    driver.get(xml_location)

    time.sleep(5)
    print("  Downloading xml for " + xml_prefix)
    out_f = os.path.join(output_dir, xml_prefix + ".xml")

    with open(out_f, "wb") as f:
        f.write(driver.page_source.encode("utf-8"))

    driver.quit()


def main():
    username = sys.argv[1]
    pw = sys.argv[2]
    output_dir = "export_xml"
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    archivist_download_xml(output_dir, username, pw)


if __name__ == "__main__":
    main()

